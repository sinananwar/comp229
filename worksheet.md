# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A? farleyknight
  * Who made the first commit to repository A? scott Chacon
  * Who made the first and last commits to repository B? Shawn O. Pearce
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened? No, last update was back in 2012 for both. the version of both files where final
  * 🤔 Which file in each project has had the most activity? the main.py and ruby-git.

# Task 2

Setup a new IntelliJ project with a main method that will print the following message to the console when run:

System.out.println("Sheeps and Wolves");
~~~~~
Sheep and Wolves
~~~~~

🤔 Now setup a new bitbucket repository and have this project pushed to that repository.
You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore`
file.  The one we have in this repository is a very good start.
